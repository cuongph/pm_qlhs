﻿USE [QLHS]
GO

--TaiKhoan admim/123456
INSERT INTO TaiKhoan(TenTaiKhoan,MatKhau,LoaiTaiKhoan)
VALUES('admin', '$2a$11$IZeXRgGVLRmCOvNX6rVm8ePS.cfdKwUcqUa1iXKxYG0Ffi1QGW7Mq', 0)

--Khoi
INSERT INTO Khoi(TenKhoi)
VALUES (N'Khối 10'),(N'Khối 11'),(N'Khối 12')

--MonHoc
INSERT INTO MonHoc(TenMonHoc)
VALUES (N'Toán'),(N'Ngữ văn'),(N'Lịch sử')

--HocKy
INSERT INTO HocKy(TenHocKy)
VALUES (N'Học kỳ 1'),(N'Học kỳ 2')

GO