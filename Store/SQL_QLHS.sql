USE [master]
GO
/****** Object:  Database [QLHS]    Script Date: 30/08/2022 12:20:18 SA ******/
CREATE DATABASE [QLHS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLHS', FILENAME = N'D:\ProgramData\Microsoft SQL Server\MSAS15.MSSQLSERVER\MSSQL\DATA\QLHS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QLHS_log', FILENAME = N'D:\ProgramData\Microsoft SQL Server\MSAS15.MSSQLSERVER\MSSQL\DATA\QLHS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [QLHS] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLHS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLHS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLHS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLHS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLHS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLHS] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLHS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLHS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLHS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLHS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLHS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLHS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLHS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLHS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLHS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLHS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLHS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLHS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLHS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLHS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLHS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLHS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLHS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLHS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLHS] SET  MULTI_USER 
GO
ALTER DATABASE [QLHS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLHS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLHS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLHS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QLHS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [QLHS] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [QLHS] SET QUERY_STORE = OFF
GO
USE [QLHS]
GO
/****** Object:  Table [dbo].[BanGiamHieu]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BanGiamHieu](
	[MaBanGiamHieu] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](200) NULL,
	[MaTaiKhoan] [int] NULL,
 CONSTRAINT [PK_BanGiamHieu] PRIMARY KEY CLUSTERED 
(
	[MaBanGiamHieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietPhieuMoi]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhieuMoi](
	[MaChiTietPhieuMoi] [int] IDENTITY(1,1) NOT NULL,
	[MaPhieuMoi] [int] NULL,
	[MaHocSinh] [int] NULL,
 CONSTRAINT [PK_ChiTietPhieuMoi] PRIMARY KEY CLUSTERED 
(
	[MaChiTietPhieuMoi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Diem]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Diem](
	[MaDiem] [int] IDENTITY(1,1) NOT NULL,
	[Diem15Phut] [real] NULL,
	[Diem1Tiet] [real] NULL,
	[DiemHocKy] [real] NULL,
 CONSTRAINT [PK_BangDiem] PRIMARY KEY CLUSTERED 
(
	[MaDiem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DiemMon]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiemMon](
	[MaBangDiemMon] [int] IDENTITY(1,1) NOT NULL,
	[MaMonHoc] [int] NULL,
	[MaHocSinh] [int] NULL,
	[MaDiem] [int] NULL,
	[MaHocKy] [int] NULL,
 CONSTRAINT [PK_BangDiemMon] PRIMARY KEY CLUSTERED 
(
	[MaBangDiemMon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiaoVien]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiaoVien](
	[MaGiaoVien] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](200) NULL,
	[ChucVu] [nvarchar](150) NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_GiaoVien] PRIMARY KEY CLUSTERED 
(
	[MaGiaoVien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiaoVu]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiaoVu](
	[MaGiaoVu] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](200) NULL,
	[MaTaiKhoan] [int] NULL,
	[MaKhoi] [int] NULL,
 CONSTRAINT [PK_GiaoVu] PRIMARY KEY CLUSTERED 
(
	[MaGiaoVu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HocKy]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HocKy](
	[MaHocKy] [int] IDENTITY(1,1) NOT NULL,
	[TenHocKy] [nvarchar](50) NULL,
 CONSTRAINT [PK_HocKy] PRIMARY KEY CLUSTERED 
(
	[MaHocKy] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HocSinh]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HocSinh](
	[MaHocSinh] [int] IDENTITY(1,1) NOT NULL,
	[MaLop] [int] NULL,
	[MaTaiKhoan] [int] NULL,
	[HoTen] [nvarchar](200) NULL,
	[NgaySinh] [date] NULL,
	[GioiTinh] [bit] NULL,
	[Email] [nvarchar](200) NULL,
	[DiaChi] [nvarchar](500) NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_HocSinh] PRIMARY KEY CLUSTERED 
(
	[MaHocSinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Khoi]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Khoi](
	[MaKhoi] [int] IDENTITY(1,1) NOT NULL,
	[TenKhoi] [nvarchar](50) NULL,
 CONSTRAINT [PK_Khoi] PRIMARY KEY CLUSTERED 
(
	[MaKhoi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lop]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lop](
	[MaLop] [int] IDENTITY(1,1) NOT NULL,
	[TenLop] [nvarchar](200) NULL,
	[SiSo] [nvarchar](50) NULL,
	[MaKhoi] [int] NULL,
	[MaGVChuNhiem] [int] NULL,
	[ThoiKhoaBieu] [xml] NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_BangLop] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonHoc]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonHoc](
	[MaMonHoc] [int] IDENTITY(1,1) NOT NULL,
	[TenMonHoc] [nvarchar](200) NULL,
 CONSTRAINT [PK_MonHoc] PRIMARY KEY CLUSTERED 
(
	[MaMonHoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhieuMoi]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuMoi](
	[MaPhieuMoi] [int] IDENTITY(1,1) NOT NULL,
	[TenPhieuMoi] [nvarchar](200) NULL,
	[NoiDungPhieuMoi] [text] NULL,
 CONSTRAINT [PK_PhieuMoi] PRIMARY KEY CLUSTERED 
(
	[MaPhieuMoi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuyDinh]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyDinh](
	[MaQuyDinh] [int] IDENTITY(1,1) NOT NULL,
	[TenQuyDinh] [nvarchar](200) NULL,
	[NoiDungQuyDinh] [nvarchar](max) NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_QuyDinh] PRIMARY KEY CLUSTERED 
(
	[MaQuyDinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[MaTaiKhoan] [int] IDENTITY(1,1) NOT NULL,
	[TenTaiKhoan] [nvarchar](50) NULL,
	[MatKhau] [nvarchar](200) NULL,
	[LoaiTaiKhoan] [int] NULL,
	[MaGiaoVien] [int] NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[MaTaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThongBao]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongBao](
	[MaThongBao] [int] IDENTITY(1,1) NOT NULL,
	[TenThongBao] [nvarchar](200) NULL,
	[NoiDungThongBao] [nvarchar](max) NULL,
	[NgayThongBao] [datetime] NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_ThongBao] PRIMARY KEY CLUSTERED 
(
	[MaThongBao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChiTietPhieuMoi]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuMoi_HocSinh] FOREIGN KEY([MaHocSinh])
REFERENCES [dbo].[HocSinh] ([MaHocSinh])
GO
ALTER TABLE [dbo].[ChiTietPhieuMoi] CHECK CONSTRAINT [FK_ChiTietPhieuMoi_HocSinh]
GO
ALTER TABLE [dbo].[ChiTietPhieuMoi]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuMoi_PhieuMoi] FOREIGN KEY([MaPhieuMoi])
REFERENCES [dbo].[PhieuMoi] ([MaPhieuMoi])
GO
ALTER TABLE [dbo].[ChiTietPhieuMoi] CHECK CONSTRAINT [FK_ChiTietPhieuMoi_PhieuMoi]
GO
ALTER TABLE [dbo].[DiemMon]  WITH CHECK ADD  CONSTRAINT [FK_DiemMon_Diem] FOREIGN KEY([MaDiem])
REFERENCES [dbo].[Diem] ([MaDiem])
GO
ALTER TABLE [dbo].[DiemMon] CHECK CONSTRAINT [FK_DiemMon_Diem]
GO
ALTER TABLE [dbo].[DiemMon]  WITH CHECK ADD  CONSTRAINT [FK_DiemMon_HocKy] FOREIGN KEY([MaHocKy])
REFERENCES [dbo].[HocKy] ([MaHocKy])
GO
ALTER TABLE [dbo].[DiemMon] CHECK CONSTRAINT [FK_DiemMon_HocKy]
GO
ALTER TABLE [dbo].[DiemMon]  WITH CHECK ADD  CONSTRAINT [FK_DiemMon_HocSinh] FOREIGN KEY([MaHocSinh])
REFERENCES [dbo].[HocSinh] ([MaHocSinh])
GO
ALTER TABLE [dbo].[DiemMon] CHECK CONSTRAINT [FK_DiemMon_HocSinh]
GO
ALTER TABLE [dbo].[DiemMon]  WITH CHECK ADD  CONSTRAINT [FK_DiemMon_MonHoc] FOREIGN KEY([MaMonHoc])
REFERENCES [dbo].[MonHoc] ([MaMonHoc])
GO
ALTER TABLE [dbo].[DiemMon] CHECK CONSTRAINT [FK_DiemMon_MonHoc]
GO
ALTER TABLE [dbo].[GiaoVu]  WITH CHECK ADD  CONSTRAINT [FK_GiaoVu_Khoi] FOREIGN KEY([MaKhoi])
REFERENCES [dbo].[Khoi] ([MaKhoi])
GO
ALTER TABLE [dbo].[GiaoVu] CHECK CONSTRAINT [FK_GiaoVu_Khoi]
GO
ALTER TABLE [dbo].[HocSinh]  WITH CHECK ADD  CONSTRAINT [FK_HocSinh_Lop] FOREIGN KEY([MaLop])
REFERENCES [dbo].[Lop] ([MaLop])
GO
ALTER TABLE [dbo].[HocSinh] CHECK CONSTRAINT [FK_HocSinh_Lop]
GO
ALTER TABLE [dbo].[HocSinh]  WITH CHECK ADD  CONSTRAINT [FK_HocSinh_TaiKhoan] FOREIGN KEY([MaTaiKhoan])
REFERENCES [dbo].[TaiKhoan] ([MaTaiKhoan])
GO
ALTER TABLE [dbo].[HocSinh] CHECK CONSTRAINT [FK_HocSinh_TaiKhoan]
GO
ALTER TABLE [dbo].[Lop]  WITH CHECK ADD  CONSTRAINT [FK_Lop_GiaoVien] FOREIGN KEY([MaGVChuNhiem])
REFERENCES [dbo].[GiaoVien] ([MaGiaoVien])
GO
ALTER TABLE [dbo].[Lop] CHECK CONSTRAINT [FK_Lop_GiaoVien]
GO
ALTER TABLE [dbo].[Lop]  WITH CHECK ADD  CONSTRAINT [FK_Lop_Khoi] FOREIGN KEY([MaKhoi])
REFERENCES [dbo].[Khoi] ([MaKhoi])
GO
ALTER TABLE [dbo].[Lop] CHECK CONSTRAINT [FK_Lop_Khoi]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_GiaoVien] FOREIGN KEY([MaGiaoVien])
REFERENCES [dbo].[GiaoVien] ([MaGiaoVien])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_GiaoVien]
GO
/****** Object:  StoredProcedure [dbo].[sp_Diem]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Diem]
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,

	@MaDiem int= null,
	@Diem15Phut real = null,
	@Diem1Tiet real = null,
	@DiemHocKy real = null,
	@MaMonHoc int = null,
	@MaHocSinh int =null,
	@MaHocKy int = null,
	@MaLop int = null
AS
BEGIN
	SET NOCOUNT ON;

	IF @Activity = 'GetAll'
	BEGIN
		select d.MaDiem, d.Diem15Phut, d.Diem1Tiet, d.DiemHocKy, dm.MaBangDiemMon, dm.MaHocKy, dm.MaHocSinh, dm.MaMonHoc, 
		mh.TenMonHoc, hs.HoTen, hk.TenHocKy, l.TenLop
		from Diem as d
		left join DiemMon as dm on d.MaDiem = dm.MaDiem
		left join MonHoc as mh on mh.MaMonHoc = dm.MaMonHoc
		left join HocSinh as hs on hs.MaHocSinh = dm.MaHocSinh
		left join HocKy as hk on hk.MaHocKy = dm.MaHocKy
		left join Lop as l on l.MaLop = hs.MaLop
		where isnull(hs.TrangThai,0) <> 1 and isnull(l.TrangThai,0) <> 1 and 
			((@MaLop is null and @MaHocSinh is null) or
			(@MaLop is not null and @MaHocSinh is not null and hs.MaLop = @MaLop and dm.MaHocSinh = @MaHocSinh) or
			(@MaLop is null and @MaHocSinh is not null and dm.MaHocSinh = @MaHocSinh) or
			(@MaLop is not null and @MaHocSinh is null and hs.MaLop = @MaLop))
		order by dm.MaHocSinh, dm.MaMonHoc, dm.MaHocKy
	END

	IF @Activity = 'GetById'
	BEGIN
		select d.MaDiem, d.Diem15Phut, d.Diem1Tiet, d.DiemHocKy, dm.MaBangDiemMon, dm.MaHocKy, dm.MaHocSinh, dm.MaMonHoc
		from Diem as d
		left join DiemMon as dm on d.MaDiem = dm.MaDiem
		where d.MaDiem = @MaDiem
	END

    IF @Activity = 'Create'
	BEGIN
		INSERT INTO Diem(
			Diem15Phut,
			Diem1Tiet,
			DiemHocKy
		)
		values(
			@Diem15Phut,
			@Diem1Tiet,
			@DiemHocKy
		)

		SET @ReturnID = SCOPE_IDENTITY()

		insert into DiemMon(
			MaMonHoc,
			MaHocSinh,
			MaDiem,
			MaHocKy
		)
		values(
			@MaMonHoc,
			@MaHocSinh,
			@ReturnID,
			@MaHocKy
		)
	END

	If @Activity = 'Delete'
	begin
		if EXISTS(select 1 from Diem where MaDiem = @MaDiem)
		begin
			delete from DiemMon where MaDiem = @MaDiem
			delete from Diem where MaDiem = @MaDiem
			
			SET @ReturnID = 1
		end
		else
		begin
			SET @ReturnID = 0
		end
	end

	IF @Activity = 'Update'
	BEGIN
		if EXISTS(select 1 from Diem where MaDiem = @MaDiem)
		begin
			update Diem set Diem15Phut = @Diem15Phut, Diem1Tiet = @Diem1Tiet, DiemHocKy = @DiemHocKy
			where MaDiem = @MaDiem

			update DiemMon set MaHocKy = @MaHocKy, MaHocSinh = @MaHocSinh, MaMonHoc = @MaMonHoc
			where MaDiem = @MaDiem

			SET @ReturnID = 1
		end
		else
		begin
			SET @ReturnID = 0
		end
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GiaoVien]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- GVien: Giáo viên
-- GVu: Giao vụ
-- BGH: Ban giám hiệu
CREATE PROCEDURE [dbo].[sp_GiaoVien] 
	--Common
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,
	--
	@MaGiaoVien INT = NULL,
	@HoTen NVARCHAR(200) = NULL,
	@ChucVu NVARCHAR(150) = NULL,
	@LoaiTaiKhoan INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @Activity = 'GetAll'
	BEGIN
		SELECT gv.MaGiaoVien, gv.HoTen, gv.ChucVu,
			CASE 
				When gv.ChucVu = 'GVien' then N'Giáo Viên'
				When gv.ChucVu = 'GVu' then N'Giáo Vụ'
				When gv.ChucVu = 'BGH' then N'Ban Giám Hiệu'
				else N'Giáo Viên'
			END as TenChucVu,
			tk.MaTaiKhoan,
			tk.TenTaiKhoan,
			tk.LoaiTaiKhoan,
			CASE 
				When tk.LoaiTaiKhoan = 2 then N'Giáo Viên'
				When tk.LoaiTaiKhoan = 3 then N'Giáo Vụ'
				When tk.LoaiTaiKhoan = 1 then N'Ban Giám Hiệu'
				else N''
			END as TenLoaiTaiKhoan
		FROM GiaoVien (NOLOCK) as gv
		LEFT JOIN TaiKhoan as tk WITH(NOLOCK) ON tk.MaGiaoVien = gv.MaGiaoVien
		WHERE ISNULL(gv.TrangThai,0) <> 1
		AND (@HoTen IS NULL OR gv.HoTen LIKE '%' + @HoTen + '%')
	END

    IF @Activity = 'GetByID'
	BEGIN
		SELECT MaGiaoVien, HoTen, ChucVu,
		CASE 
			When ChucVu = 'GVien' then N'Giáo Viên'
			When ChucVu = 'GVu' then N'Giáo Vụ'
			When ChucVu = 'BGH' then N'Ban Giám Hiệu'
			else N'Giáo Viên'
		END as TenChucVu
		FROM GiaoVien (NOLOCK)
		WHERE MaGiaoVien = @MaGiaoVien
		AND ISNULL(TrangThai,0) <> 1
	END
	IF @Activity = 'Create'
	BEGIN
		INSERT INTO GiaoVien (
			HoTen,
			ChucVu
		) VALUES (
			@HoTen,
			@ChucVu
		)

		SET @ReturnID = SCOPE_IDENTITY()
	END
	IF @Activity = 'Update'
	BEGIN
		UPDATE GiaoVien SET
			HoTen = @HoTen,
			ChucVu = @ChucVu
		WHERE
			MaGiaoVien = @MaGiaoVien

		SET @ReturnID = @@ROWCOUNT
	END

	IF @Activity = 'Delete'
	BEGIN
		UPDATE GiaoVien SET
			TrangThai = 1
		WHERE
			MaGiaoVien = @MaGiaoVien

		--
		UPDATE TaiKhoan SET
			TrangThai = 1
		WHERE
			MaGiaoVien = @MaGiaoVien

		SET @ReturnID = @@ROWCOUNT
	END

	IF @Activity = 'GetAllGVCN'
	BEGIN
		SELECT MaGiaoVien, HoTen
		FROM GiaoVien (NOLOCK)
		WHERE ISNULL(TrangThai,0) <> 1 and ChucVu = 'GVien'
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_HocKy]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_HocKy]
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

   IF @Activity = 'GetAll'
	BEGIN
		SELECT *
		from HocKy
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_HocSinh]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_HocSinh]
  @Activity VARCHAR(100) = NULL,
  @AccountLogin VARCHAR(20) = NULL,
  @ReturnMess NVARCHAR(2000) = NULL OUT,
  @ReturnStrID NVARCHAR(50) = NULL OUT,
  @ReturnID int = NULL OUTPUT,

  @MaLop int = null,
  @HoTen nvarchar(200)=null,
  @NgaySinh datetime=null,
  @GioiTinh bit = null,
  @Email nvarchar(200) = null,
  @DiaChi nvarchar(200)=null,
  @MaHocSinh int = null
AS
BEGIN
	
	SET NOCOUNT ON;

	IF @Activity = 'GetAll'
	BEGIN
		SELECT hs.*, l.TenLop
		FROM HocSinh as hs (NOLOCK)
		left join Lop as l on l.MaLop = hs.MaLop
		where  isnull(hs.TrangThai,0) <> 1 and isnull(l.TrangThai,0) <> 1
		AND (@HoTen IS NULL OR hs.HoTen LIKE '%' + @HoTen + '%')
	END

	IF @Activity = 'GetById'
	BEGIN
		SELECT hs.*
		FROM HocSinh as hs (NOLOCK)
		where hs.MaHocSinh = @MaHocSinh and isnull(hs.TrangThai,0) <> 1
	END

	IF @Activity = 'Create'
	BEGIN
		INSERT INTO HocSinh(
			MaLop,
			HoTen,
			NgaySinh,
			GioiTinh,
			Email,
			DiaChi
		) VALUES (
			@MaLop,
			@HoTen,
			@NgaySinh,
			@GioiTinh,
			@Email,
			@DiaChi
		)

		SET @ReturnID = SCOPE_IDENTITY()
	END

	If @Activity = 'Delete'
	begin
		if EXISTS(select 1 from HocSinh where MaHocSinh = @MaHocSinh)
		begin
			update HocSinh set TrangThai = 1 where MaHocSinh = @MaHocSinh
			SET @ReturnID = 1
		end
		else
		begin
			SET @ReturnID = 0
		end
	end

	IF @Activity = 'Update'
	BEGIN
		if EXISTS(select 1 from HocSinh where MaHocSinh = @MaHocSinh)
		begin
			update HocSinh set MaLop = @MaLop, HoTen= @HoTen, NgaySinh = @NgaySinh, GioiTinh = @GioiTinh, Email = @Email, DiaChi = @DiaChi
			where MaHocSinh = @MaHocSinh
			SET @ReturnID = 1
		end
		else
		begin
			SET @ReturnID = 0
		end
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Khoi]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Khoi]
		--Common
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,

	@MaKhoi int= null
AS
BEGIN
	
	SET NOCOUNT ON;

   IF @Activity = 'GetAll'
	BEGIN
		SELECT *
		FROM Khoi (NOLOCK)
	END

	IF @Activity = 'GetById'
	BEGIN
		SELECT hs.*
		FROM Khoi as hs (NOLOCK)
		where hs.MaKhoi = @MaKhoi
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Lop]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Lop]
	--Common
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,

	@MaLop int = null,
	@TenLop nvarchar(200) = null,
	@SiSo nvarchar(50) = null,
	@MaKhoi int = null,
	@MaGVChuNhiem int = null,
	@ThoiKhoaBieu xml = null
AS
BEGIN
	SET NOCOUNT ON;

   IF @Activity = 'GetAll'
	BEGIN
		SELECT l.MaLop, l.TenLop, l.SiSo, l.MaKhoi, l.MaGVChuNhiem, l.ThoiKhoaBieu, k.TenKhoi, gv.HoTen
		FROM Lop as l(NOLOCK)
		left join Khoi as k on k.MaKhoi = l.MaKhoi
		left join GiaoVien as gv on gv.MaGiaoVien = l.MaGVChuNhiem
		where isnull(l.TrangThai,0) <> 1
	END

	IF @Activity = 'GetById'
	BEGIN
		SELECT MaLop, TenLop, SiSo, MaKhoi, MaGVChuNhiem, ThoiKhoaBieu
		FROM Lop (NOLOCK)
		where MaLop = @MaLop and  isnull(TrangThai,0) <> 1
	END

	IF @Activity = 'Create'
	BEGIN
		INSERT INTO Lop (
			TenLop,
			SiSo,
			MaKhoi,
			MaGVChuNhiem,
			ThoiKhoaBieu
		)
		values(
			@TenLop,
			@SiSo,
			@MaKhoi,
			@MaGVChuNhiem,
			@ThoiKhoaBieu
		)

		SET @ReturnID = SCOPE_IDENTITY()
	END

	If @Activity = 'Delete'
	begin
		if EXISTS(select 1 from Lop where MaLop = @MaLop)
		begin
			update Lop set TrangThai = 1 where MaLop = @MaLop
			SET @ReturnID = 1
		end
		else
		begin
			SET @ReturnID = 0
		end
	end

	IF @Activity = 'Update'
	BEGIN
		if EXISTS(select 1 from Lop where MaLop = @MaLop)
		begin
			update Lop set TenLop = @TenLop, SiSo = @SiSo, MaKhoi =  @MaKhoi, MaGVChuNhiem = @MaGVChuNhiem, ThoiKhoaBieu = @ThoiKhoaBieu
			where MaLop = @MaLop
			SET @ReturnID = 1
		end
		else
		begin
			SET @ReturnID = 0
		end
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MonHoc]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MonHoc]
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @Activity = 'GetAll'
	BEGIN
		SELECT *
		from MonHoc
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuyDinh]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_QuyDinh]
	--Common
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,
	--
	@MaQuyDinh INT = NULL,
	@TenQuyDinh NVARCHAR(200) = NULL,
	@NoiDungQuyDinh NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @Activity = 'GetAll'
	BEGIN
		SELECT MaQuyDinh, TenQuyDinh, NoiDungQuyDinh
		FROM QuyDinh (NOLOCK)
		WHERE ISNULL(TrangThai,0) <> 1
	END

    IF @Activity = 'GetByID'
	BEGIN
		SELECT MaQuyDinh, TenQuyDinh, NoiDungQuyDinh
		FROM QuyDinh (NOLOCK)
		WHERE MaQuyDinh = @MaQuyDinh
		AND ISNULL(TrangThai,0) <> 1
	END
	IF @Activity = 'Create'
	BEGIN
		INSERT INTO QuyDinh (
			TenQuyDinh,
			NoiDungQuyDinh
		) VALUES (
			@TenQuyDinh,
			@NoiDungQuyDinh
		)

		SET @ReturnID = SCOPE_IDENTITY()
	END
	IF @Activity = 'Update'
	BEGIN
		UPDATE QuyDinh SET
			TenQuyDinh = @TenQuyDinh,
			NoiDungQuyDinh = @NoiDungQuyDinh
		WHERE
			MaQuyDinh = @MaQuyDinh

		SET @ReturnID = @@ROWCOUNT
	END

	IF @Activity = 'Delete'
	BEGIN
		UPDATE QuyDinh SET
			TrangThai = 1
		WHERE
			MaQuyDinh = @MaQuyDinh

		SET @ReturnID = @@ROWCOUNT
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TaiKhoan]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- 1: BGH
-- 2: Giáo viên
-- 3: Giáo vụ
CREATE PROCEDURE [dbo].[sp_TaiKhoan]
	--Common
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,
	--
	@MaTaiKhoan INT = NULL,
	@TenTaiKhoan NVARCHAR(50) = NULL,
	@MatKhau NVARCHAR(200) = NULL,
	@LoaiTaiKhoan INT = NULL,
	@MaGiaoVien INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @Activity = 'GetAll'
	BEGIN
		SELECT MaTaiKhoan, TenTaiKhoan, LoaiTaiKhoan
		FROM TaiKhoan (NOLOCK)
	END

    IF @Activity = 'GetByID'
	BEGIN
		SELECT MaTaiKhoan, TenTaiKhoan, MatKhau, isnull(LoaiTaiKhoan,0) as LoaiTaiKhoan
		FROM TaiKhoan (NOLOCK)
		WHERE TenTaiKhoan = @TenTaiKhoan
		AND ISNULL(TrangThai,0) <> 1
	END

	IF @Activity = 'Create'
	BEGIN
		INSERT INTO TaiKhoan (
			TenTaiKhoan,
			MatKhau,
			LoaiTaiKhoan,
			MaGiaoVien
		) VALUES (
			@TenTaiKhoan,
			@MatKhau,
			@LoaiTaiKhoan,
			@MaGiaoVien
		)

		SET @ReturnID = SCOPE_IDENTITY()
	END

	IF @Activity = 'Update'
	BEGIN
		UPDATE TaiKhoan SET
			LoaiTaiKhoan = @LoaiTaiKhoan
		WHERE
			MaTaiKhoan = @MaTaiKhoan

		SET @ReturnID = @@ROWCOUNT
	END

	IF @Activity = 'ChangePassword'
	BEGIN
		UPDATE TaiKhoan SET
			MatKhau = @MatKhau
		WHERE
			MaTaiKhoan = @MaTaiKhoan

		SET @ReturnID = @@ROWCOUNT
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ThongBao]    Script Date: 30/08/2022 12:20:18 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ThongBao]
	--Common
	@Activity VARCHAR(100) = NULL,
	@AccountLogin VARCHAR(20) = NULL,
	@ReturnMess NVARCHAR(2000) = NULL OUT,
	@ReturnStrID NVARCHAR(50) = NULL OUT,
	@ReturnID int = NULL OUTPUT,
	--
	@MaThongBao INT = NULL,
	@TenThongBao NVARCHAR(200) = NULL,
	@NoiDungThongBao NVARCHAR(MAX) = NULL,
	@NgayThongBao DateTime = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF @Activity = 'GetAll'
	BEGIN
		SELECT MaThongBao, TenThongBao, NoiDungThongBao, Convert(varchar(10),NgayThongBao,103) as NgayThongBao
		FROM ThongBao (NOLOCK)
		WHERE ISNULL(TrangThai,0) <> 1
	END

    IF @Activity = 'GetByID'
	BEGIN
		SELECT MaThongBao, TenThongBao, NoiDungThongBao, Convert(varchar(10),NgayThongBao,103) as NgayThongBao
		FROM ThongBao (NOLOCK)
		WHERE MaThongBao = @MaThongBao
		AND ISNULL(TrangThai,0) <> 1
	END
	IF @Activity = 'Create'
	BEGIN
		INSERT INTO ThongBao (
			TenThongBao,
			NoiDungThongBao,
			NgayThongBao
		) VALUES (
			@TenThongBao,
			@NoiDungThongBao,
			GETDATE()
		)

		SET @ReturnID = SCOPE_IDENTITY()
	END
	IF @Activity = 'Update'
	BEGIN
		UPDATE ThongBao SET
			TenThongBao = @TenThongBao,
			NoiDungThongBao = @NoiDungThongBao,
			NgayThongBao = GETDATE()
		WHERE
			MaThongBao = @MaThongBao

		SET @ReturnID = @@ROWCOUNT
	END

	IF @Activity = 'Delete'
	BEGIN
		UPDATE ThongBao SET
			TrangThai = 1
		WHERE
			MaThongBao = @MaThongBao

		SET @ReturnID = @@ROWCOUNT
	END
END
GO
USE [master]
GO
ALTER DATABASE [QLHS] SET  READ_WRITE 
GO
