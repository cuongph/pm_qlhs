﻿using PM_QLHS.Class.Dtos;

namespace PM_QLHS.Models
{
    public class TaiKhoanViewModel
    {
        public int MaTaiKhoan { get; set; }
        public string? TenTaiKhoan { get; set; }
        public string? LoaiTaiKhoan { get; set; }
        public PageReponResult<TaiKhoanViewModel> TaiKhoans { get; set; }

        public TaiKhoanViewModel()
        {
            TaiKhoans = new PageReponResult<TaiKhoanViewModel>();
        }
    }
}
