﻿using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Models
{
    public class GiaoVienBindingVm
    {
        public int MaGiaoVien { get; set; }
        public string? HoTen { get; set; }
        public string? ChucVu { get; set; }
        public string? TenChucVu { get; set; }
        public int? MaTaiKhoan { get; set; }
        public string? TenTaiKhoan { get; set; }
        public int? LoaiTaiKhoan { get; set; }
        public string? TenLoaiTaiKhoan { get; set; }
    }

    public class GiaoVienViewModel
    {
        public string? HoTen { get; set; }
        public PageReponResult<GiaoVienBindingVm> GiaoViens { get; set; }

        public GiaoVienViewModel()
        {
            GiaoViens = new PageReponResult<GiaoVienBindingVm>();
        }
    }
}
