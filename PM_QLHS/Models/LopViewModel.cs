﻿using PM_QLHS.Class.Dtos;

namespace PM_QLHS.Models
{
    public class LopViewModel
    {
        public int MaLop { get; set; }
        public string? TenLop { get; set; }
        public int? SiSo { get; set; }
        public int? MaKhoi { get; set; }
        public int? MaGVChuNhiem { get; set; }
        public string? ThoiKhoaBieu { get; set; }
        public string? TenKhoi { get; set; }
        public string? HoTen { get; set; }
        public PageReponResult<LopViewModel> Lops { get; set; }

        public LopViewModel()
        {
            Lops = new PageReponResult<LopViewModel>();
        }
    }
}
