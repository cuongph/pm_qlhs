﻿using PM_QLHS.Class.Dtos;

namespace PM_QLHS.Models
{
    public class HocSinhViewModel
    {
        public int MaHocSinh { get; set; }
        public int? MaLop { get; set; }
        public int? MaTaiKhoan { get; set; }
        public string? HoTen { get; set; }
        public DateTime? NgaySinh { get; set; }
        public bool? GioiTinh { get; set; }
        public string? Email { get; set; }
        public string? DiaChi { get; set; }
        public string? TenLop { get; set; }
        public PageReponResult<HocSinhViewModel> HocSinhs { get; set; }

        public HocSinhViewModel()
        {
            HocSinhs = new PageReponResult<HocSinhViewModel>();
        }
    }
}
