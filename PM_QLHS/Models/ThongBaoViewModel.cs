﻿using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Models
{
    public class ThongBaoViewModel
    {
        public PageReponResult<ThongBaoVm> ThongBaos { get; set; }

        public ThongBaoViewModel()
        {
            ThongBaos = new PageReponResult<ThongBaoVm>();
        }
    }
}
