﻿using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Models
{
    public class QuyDinhViewModel
    {
        public PageReponResult<QuyDinhVm> QuyDinhs { get; set; }

        public QuyDinhViewModel()
        {
            QuyDinhs = new PageReponResult<QuyDinhVm>();
        }
    }
}
