﻿using PM_QLHS.Class.Dtos;

namespace PM_QLHS.Models
{
    public class DiemViewModel
    {
        public int MaBangDiemMon { get; set; }
        public int? MaMonHoc { get; set; }
        public int? MaLop { get; set; }
        public int? MaHocSinh { get; set; }
        public int? MaDiem { get; set; }
        public int? MaHocKy { get; set; }
        public float? Diem15Phut { get; set; }
        public float? Diem1Tiet { get; set; }
        public float? DiemHocKy { get; set; }
        public string? TenMonHoc { get; set; }
        public string? HoTen { get; set; }
        public string? TenHocKy { get; set; }
        public string? TenLop { get; set; }
        public PageReponResult<DiemViewModel> Diems { get; set; }

        public DiemViewModel()
        {
            Diems = new PageReponResult<DiemViewModel>();
        }
    }
}
