﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class MonHocController : Controller
    {
        private readonly ILogger<LopController> _logger;
        private readonly IMonHocRepository _repository;
        private readonly IToastNotification _toastNotification;

        public MonHocController(ILogger<LopController> logger, IToastNotification toastNotification, IMonHocRepository repository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _repository = repository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetALl()
        {
            var result = _repository.GetAll();
            return Json(result);
        }
    }
}
