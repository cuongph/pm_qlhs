﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Class;
using System.Security.Claims;
using PM_QLHS.Repository;
using Microsoft.Extensions.Configuration;
using NToastNotify;
using static PM_QLHS.Class.SystemConstants;

namespace PM_QLHS.Controllers
{
    public class ChangePasswordController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly ITaiKhoanRepository _taiKhoanRepository;
        private readonly IConfiguration _configuration;

        public ChangePasswordController(ILogger<HomeController> logger,
            IToastNotification toastNotification,
            ITaiKhoanRepository taiKhoanRepository,
            IConfiguration configuration
            )
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _taiKhoanRepository = taiKhoanRepository;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(TaiKhoanVm request)
        {
            var result = _taiKhoanRepository.ChangePass(request);
            if (result.Success)
            {
                //logout
                HttpContext.Session.Remove(MemberSession);
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("Login", "Home");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Đổi mật khẩu không thành công!");
                return View();
            }
        }
    }
}
