﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Class.Authorization;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class QuyDinhController : BaseController
    {
        private readonly ILogger<HocSinhController> _logger;
        private readonly IQuyDinhRepository _quyDinhRepository;
        private readonly IToastNotification _toastNotification;

        public QuyDinhController(ILogger<HocSinhController> logger,
            IToastNotification toastNotification,
            IQuyDinhRepository quyDinhRepository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _quyDinhRepository = quyDinhRepository;
        }

        [Authorize(Role.BGH)]
        public IActionResult Index()
        {
            var data = new QuyDinhViewModel();
            var result = _quyDinhRepository.GetAll();
            data.QuyDinhs = result.Success ? result.Data : new PageReponResult<QuyDinhVm>();
            return View(data);
        }

        [HttpGet]
        [Route("/quydinh/getById")]
        public IActionResult GetById(int id)
        {
            var result = _quyDinhRepository.GetById(id);
            return Ok(result);
        }

        [HttpPost]
        [Route("/quydinh/create")]
        public IActionResult Create(QuyDinhVm vm)
        {
            var result = _quyDinhRepository.Create(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        [HttpPatch]
        [Route("/quydinh/update")]
        public IActionResult Update(QuyDinhVm vm)
        {
            if (vm.MaQuyDinh == null || vm.MaQuyDinh <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _quyDinhRepository.Update(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (id <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _quyDinhRepository.Delete(id);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Xóa dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Xóa dữ liệu thất bại!");
            }
            return RedirectToAction(nameof(Index));
        }

    }
}
