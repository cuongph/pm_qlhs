﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Class.Authorization;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class ThongBaoController : BaseController
    {
        private readonly ILogger<HocSinhController> _logger;
        private readonly IThongBaoRepository _thongBaoRepository;
        private readonly IToastNotification _toastNotification;

        public ThongBaoController(ILogger<HocSinhController> logger,
            IToastNotification toastNotification,
            IThongBaoRepository thongBaoRepository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _thongBaoRepository = thongBaoRepository;
        }

        [Authorize(Role.GVu)]
        public IActionResult Index()
        {
            var data = new ThongBaoViewModel();
            var result = _thongBaoRepository.GetAll();
            data.ThongBaos = result.Success ? result.Data : new PageReponResult<ThongBaoVm>();
            return View(data);
        }

        [HttpGet]
        [Route("/thongbao/getById")]
        public IActionResult GetById(int id)
        {
            var result = _thongBaoRepository.GetById(id);
            return Ok(result);
        }

        [HttpPost]
        [Route("/thongbao/create")]
        public IActionResult Create(ThongBaoVm vm)
        {
            var result = _thongBaoRepository.Create(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        [HttpPatch]
        [Route("/thongbao/update")]
        public IActionResult Update(ThongBaoVm vm)
        {
            if (vm.MaThongBao == null || vm.MaThongBao <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _thongBaoRepository.Update(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (id <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _thongBaoRepository.Delete(id);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Xóa dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Xóa dữ liệu thất bại!");
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
