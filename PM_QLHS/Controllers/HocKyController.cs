﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class HocKyController : Controller
    {
        private readonly ILogger<LopController> _logger;
        private readonly IHocKyRepository _repository;
        private readonly IToastNotification _toastNotification;

        public HocKyController(ILogger<LopController> logger, IToastNotification toastNotification, IHocKyRepository repository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _repository = repository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetALl()
        {
            var result = _repository.GetAll();
            return Json(result);
        }
    }
}
