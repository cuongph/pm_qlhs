﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PM_QLHS.Class;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Controllers
{
    [TypeFilter(typeof(SessionModelValidate))]
    public abstract class BaseController : Controller
    {
        public TaiKhoanVm LoggedInUserMember => HttpContext.Session.GetString(SystemConstants.MemberSession) != null ? JsonConvert.DeserializeObject<TaiKhoanVm>(HttpContext.Session.GetString(SystemConstants.MemberSession)) : null;
    }
}
