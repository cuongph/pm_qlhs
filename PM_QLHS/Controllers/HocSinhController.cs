﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Class.Authorization;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Repository;
using System.Data;
using System.Diagnostics;

namespace PM_QLHS.Controllers
{
    public class HocSinhController : BaseController
    {
        private readonly ILogger<HocSinhController> _logger;
        private readonly IHocSinhRepository _hocSinhRepository;
        private readonly IToastNotification _toastNotification;

        public HocSinhController(ILogger<HocSinhController> logger, IToastNotification toastNotification, IHocSinhRepository hocSinhRepository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _hocSinhRepository = hocSinhRepository;
        }

        [Authorize(Role.GVu)]
        public IActionResult Index()
        {
            var data = new HocSinhViewModel();
            var result = _hocSinhRepository.GetAllView("");
            data.HocSinhs = result.Success ? result.Data : new PageReponResult<HocSinhViewModel>();
            return View(data);
        }

        [HttpPost]
        public IActionResult Index(HocSinhVm request)
        {
            var data = new HocSinhViewModel();
            var result = _hocSinhRepository.GetAllView(request.HoTen);
            data.HocSinhs = result.Success ? result.Data : new PageReponResult<HocSinhViewModel>();
            return View(data);
        }

        [HttpGet]
        public ActionResult GetALl()
        {
            var result = _hocSinhRepository.GetAll();
            return Json(result);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        [Route("/hocsinh/create")]
        public IActionResult Create(HocSinhVm request)
        {
            var result = _hocSinhRepository.Create(request);

            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        [HttpPatch]
        [Route("/hocsinh/update")]
        public IActionResult Update(HocSinhVm request)
        {
            if (request.MaHocSinh <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _hocSinhRepository.Update(request);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (id <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _hocSinhRepository.Delete(id);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Xóa dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Xóa dữ liệu thất bại!");
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Route("/hocsinh/getById")]
        public IActionResult GetById(int id)
        {
            var result = _hocSinhRepository.GetById(id);
            return Json(result);
        }
    }
}
