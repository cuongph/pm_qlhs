﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NToastNotify;
using PM_QLHS.Class.Authorization;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class LopController : Controller
    {
        private readonly ILogger<LopController> _logger;
        private readonly ILopRepository _lopRepository;
        private readonly IToastNotification _toastNotification;

        public LopController(ILogger<LopController> logger, IToastNotification toastNotification, ILopRepository lopRepository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _lopRepository = lopRepository;
        }

        [Authorize(Role.GVu)]
        public IActionResult Index()
        {
            var data = new LopViewModel();
            var result = _lopRepository.GetAllView();

            data.Lops = result.Success ? result.Data : new PageReponResult<LopViewModel>();
            return View(data);
        }

        [HttpGet]
        public ActionResult GetALl()
        {
            var result = _lopRepository.GetAll();
            return Json(result);
        }

        [HttpGet]
        [Route("/lop/getById")]
        public IActionResult GetById(int id)
        {
            var result = _lopRepository.GetById(id);
            return Json(result);
        }

        [HttpPost]
        [Route("/lop/create")]
        public IActionResult Create(LopVm request)
        {
            var result = _lopRepository.Create(request);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        [HttpPatch]
        [Route("/lop/update")]
        public IActionResult Update(LopVm request)
        {
            if (request.MaLop <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _lopRepository.Update(request);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (id <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _lopRepository.Delete(id);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Xóa dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Xóa dữ liệu thất bại!");
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
