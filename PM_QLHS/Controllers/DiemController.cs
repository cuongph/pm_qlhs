﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Class.Authorization;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class DiemController : Controller
    {
        private readonly ILogger<LopController> _logger;
        private readonly IDiemRepository _diemRepository;
        private readonly IToastNotification _toastNotification;

        public DiemController(ILogger<LopController> logger, IToastNotification toastNotification, IDiemRepository diemRepository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _diemRepository = diemRepository;
        }

        [Authorize(Role.GVien)]
        public IActionResult Index()
        {
            var data = new DiemViewModel();
            var result = _diemRepository.GetAllView(new TraCuuDiemVm());

            data.Diems = result.Success ? result.Data : new PageReponResult<DiemViewModel>();
            return View(data);
        }

        public IActionResult TraCuu()
        {
            var data = new DiemViewModel();
            var result = _diemRepository.GetAllView(new TraCuuDiemVm());

            data.Diems = result.Success ? result.Data : new PageReponResult<DiemViewModel>();
            return View(data);
        }

        [HttpPost]
        public IActionResult TraCuu(TraCuuDiemVm request)
        {
            var data = new DiemViewModel();
            var result = _diemRepository.GetAllView(request);

            data.Diems = result.Success ? result.Data : new PageReponResult<DiemViewModel>();
            return View(data);
        }

        [HttpGet]
        [Route("/diem/getById")]
        public IActionResult GetById(int id)
        {
            var result = _diemRepository.GetById(id);
            return Json(result);
        }

        [HttpPost]
        [Route("/diem/create")]
        public IActionResult Create(DiemVm request)
        {
            var result = _diemRepository.Create(request);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        [HttpPatch]
        [Route("/diem/update")]
        public IActionResult Update(DiemVm request)
        {
            if (request.MaDiem <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _diemRepository.Update(request);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (id <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _diemRepository.Delete(id);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Xóa dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Xóa dữ liệu thất bại!");
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
