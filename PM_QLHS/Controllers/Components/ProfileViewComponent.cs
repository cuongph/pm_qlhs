﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PM_QLHS.Class;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Controllers.Components
{
    public class ProfileViewComponent : ViewComponent
    {
        public ProfileViewComponent()
        {
        }
        public IViewComponentResult Invoke()
        {
            try
            {
                var sessionLogin = HttpContext.Session.GetString(SystemConstants.MemberSession);
                if (sessionLogin != null)
                {
                    var member = JsonConvert.DeserializeObject<TaiKhoanVm>(sessionLogin);
                    return View(member);
                }
            }
            catch (Exception)
            {
                ViewBag.breadCrumb = "";
            }
            return View(new TaiKhoanVm());
        }
    }
}
