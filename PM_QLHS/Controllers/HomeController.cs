﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Models;
using PM_QLHS.Repository;
using System.Diagnostics;
using PM_QLHS.Class.ViewModels;
using NToastNotify;
using PM_QLHS.Class;
using Newtonsoft.Json;
using static PM_QLHS.Class.SystemConstants;
using System.Security.Claims;
using PM_QLHS.Class.Authorization;
using AuthorizeAttribute = Microsoft.AspNetCore.Authorization.AuthorizeAttribute;

namespace PM_QLHS.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly ITestRepository _testRepository;
        private readonly ITaiKhoanRepository _taiKhoanRepository;
        private readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, 
            IToastNotification toastNotification, 
            ITestRepository testRepository, 
            ITaiKhoanRepository taiKhoanRepository,
            IConfiguration configuration
            )
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _testRepository = testRepository;
            _taiKhoanRepository = taiKhoanRepository;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var user = HttpContext.Session.GetString(SystemConstants.MemberSession) != null ? JsonConvert.DeserializeObject<TaiKhoanViewModel>(HttpContext.Session.GetString(SystemConstants.MemberSession)) : null;
            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Remove(MemberSession);
            //HttpContext.Session.Remove(MainMenuSession);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public IActionResult Forbidden()
        {
            return View("/Views/Shared/Forbidden.cshtml");
        }

        [HttpGet]
        public IActionResult SessionTimeout()
        {
            return View("/Views/Shared/SessionTimeout.cshtml");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            HttpContext.Session.Remove(MemberSession);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(TaiKhoanVm request)
        {
            var result = _taiKhoanRepository.Authenticate(request);
            if (result == null)
            {
                ViewBag.error = "Đăng nhập không thành công";
                _toastNotification.AddErrorToastMessage("Đăng nhập không thành công");
                return View();
            }

            if (result.Success)
            {
                //logout xong mới login
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                var userVm = new TaiKhoanVm();
                userVm = result.Data;

                //session login
                HttpContext.Session.SetString(SystemConstants.MemberSession, JsonConvert.SerializeObject(userVm));
                //session login
                var authProperties = new AuthenticationProperties
                {
                    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(int.Parse(_configuration["TokenExpires"])),
                    IsPersistent = false
                };
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, userVm.TenTaiKhoan),
                    new Claim(ClaimTypes.Role, userVm.LoaiTaiKhoan.ToString()),
                };

                var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);
                var namePage = new PageDefault();
                return RedirectToAction("Index", namePage.Pages[userVm.LoaiTaiKhoan]);
            }
            else
            {
                ViewBag.error = result.Message;
                _toastNotification.AddErrorToastMessage(result.Message);
                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Register(TaiKhoanVm request)
        {
            var result = _taiKhoanRepository.Register(request);
            if (result == null)
            {
                ViewBag.error = "Đăng ký không thành công";
                _toastNotification.AddErrorToastMessage("Đăng ký không thành công");
                return View();
            }

            if (result.Success)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.error = result.Message;
                _toastNotification.AddErrorToastMessage(result.Message);
                return View();
            }
        }

        public RedirectToActionResult HocSinhList()
        {
            return RedirectToAction("Index", "HocSinh");
        }
        
        public RedirectToActionResult ChangePass()
        {
            return RedirectToAction("Index", "ChangePassword");
        }

        public RedirectToActionResult GiaoVienList()
        {
            return RedirectToAction("Index", "GiaoVien");
        }

        public RedirectToActionResult LopList()
        {
            return RedirectToAction("Index", "Lop");
        }

        public RedirectToActionResult QuyDinhList()
        {
            return RedirectToAction("Index", "QuyDinh");
        }

        public RedirectToActionResult ThongBaoList()
        {
            return RedirectToAction("Index", "ThongBao");
        }

        public RedirectToActionResult DiemList()
        {
            return RedirectToAction("Index", "Diem");
        }

        public RedirectToActionResult TraCuuDiem()
        {
            return RedirectToAction("TraCuu", "Diem");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}