﻿using Microsoft.AspNetCore.Mvc;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class KhoiController : Controller
    {
        private readonly ILogger<LopController> _logger;
        private readonly IKhoiRepository _khoiRepository;

        public KhoiController(ILogger<LopController> logger, IKhoiRepository khoiRepository)
        {
            _logger = logger;
            _khoiRepository = khoiRepository;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetALl()
        {
            var result = _khoiRepository.GetAll();
            return Json(result);
        }

        [HttpGet]
        [Route("/khoi/getById")]
        public IActionResult GetById(int id)
        {
            var result = _khoiRepository.GetById(id);
            return Json(result);
        }
    }
}
