﻿using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PM_QLHS.Class.Authorization;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Repository;

namespace PM_QLHS.Controllers
{
    public class GiaoVienController : BaseController
    {
        private readonly ILogger<HocSinhController> _logger;
        private readonly IGiaoVienRepository _giaoVienRepository;
        private readonly ITaiKhoanRepository _taiKhoanRepository;
        private readonly IToastNotification _toastNotification;

        public GiaoVienController(ILogger<HocSinhController> logger, 
            IToastNotification toastNotification,
            IGiaoVienRepository giaoVienRepository,
            ITaiKhoanRepository taiKhoanRepository)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _giaoVienRepository = giaoVienRepository;
            _taiKhoanRepository = taiKhoanRepository;
        }

        [Authorize(Role.GVu)]
        public IActionResult Index()
        {
            var data = new GiaoVienViewModel();
            var result = _giaoVienRepository.GetAll("");
            data.GiaoViens = result.Success ? result.Data : new PageReponResult<GiaoVienBindingVm>();
            return View(data);
        }

        [HttpPost]
        public IActionResult Index(GiaoVienVm request)
        {
            var data = new GiaoVienViewModel();
            var result = _giaoVienRepository.GetAll(request.HoTen);
            data.GiaoViens = result.Success ? result.Data : new PageReponResult<GiaoVienBindingVm>();
            return View(data);
        }

        [HttpGet]
        [Route("/giaovien/getById")]
        public IActionResult GetById(int id)
        {
            var result = _giaoVienRepository.GetById(id);
            return Ok(result);
        }
        
        [HttpGet]
        [Route("/giaovien/getTKById")]
        public IActionResult GetTKById(string id)
        {
            var result = _taiKhoanRepository.GetByID(id);
            return Ok(result);
        }

        [HttpPost]
        [Route("/giaovien/create")]
        public IActionResult Create(GiaoVienVm request)
        {
            var result = _giaoVienRepository.Create(request);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        [HttpPatch]
        [Route("/giaovien/update")]
        public IActionResult Update(GiaoVienVm vm)
        {
            if (vm.MaGiaoVien == null || vm.MaGiaoVien <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _giaoVienRepository.Update(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Lưu dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Lưu dữ liệu thất bại!");
            }
            return Ok(result);
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (id <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng nhập đầy đủ dữ liệu yêu cầu!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _giaoVienRepository.Delete(id);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Xóa dữ liệu thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Xóa dữ liệu thất bại!");
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Route("/giaovien/register")]
        public IActionResult Register(TaiKhoanVm vm)
        {
            if (vm.LoaiTaiKhoan == null || vm.LoaiTaiKhoan <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng chọn loại tài khoản!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _taiKhoanRepository.Register(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Thêm tài khoản thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Thêm tài khoản thất bại!");
            }
            return Ok(result);
        }

        [HttpPost]
        [Route("/giaovien/changerole")]
        public IActionResult ChangeRole(TaiKhoanVm vm)
        {
            if (vm.LoaiTaiKhoan == null || vm.LoaiTaiKhoan <= 0)
            {
                _toastNotification.AddErrorToastMessage("Vui lòng chọn loại tài khoản!!");
                return Ok(new ApiErrorResult<int>());
            }
            var result = _taiKhoanRepository.Update(vm);
            if (result.Success)
            {
                _toastNotification.AddSuccessToastMessage("Sửa loại tài khoản thành công!");
            }
            else
            {
                _toastNotification.AddErrorToastMessage("Sửa loại tài khoản thất bại!");
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("/giaovien/getallgvcn")]
        public ActionResult GetALl()
        {
            var result = _giaoVienRepository.GetAllGVCN();
            return Json(result);
        }
    }
}
