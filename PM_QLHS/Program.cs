﻿using Microsoft.AspNetCore.Authentication.Cookies;
using NToastNotify;
using PM_QLHS.Class;
using PM_QLHS.Repository;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
// Add services to the container.
services.AddHttpClient();
services.Configure<ConnectionStrings>(builder.Configuration.GetSection("ConnectionStrings"));
// Add services to the container.
services.AddControllersWithViews();
// Add ToastNotification
services.AddRazorPages().AddNToastNotifyNoty(new NotyOptions
{
    Layout = "topRight",
    ProgressBar = true,
    Timeout = 5000,
    Theme = "metroui"
});
services.AddResponseCaching();
int TokenExpires = int.Parse(builder.Configuration.GetValue<string>("TokenExpires").ToString());
services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        //options.ExpireTimeSpan = TimeSpan.FromMinutes(TokenExpires);
        //options.SlidingExpiration = true;
        options.LoginPath = "/Home/Login";
        options.AccessDeniedPath = "/Home/Forbidden/";
    });

services.AddDistributedMemoryCache();// Đăng ký dịch vụ lưu cache trong bộ nhớ (Session sẽ sử dụng nó)
services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(TokenExpires);
});
services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
services.AddTransient<ITestRepository, TestRepository>();
services.AddTransient<ITaiKhoanRepository, TaiKhoanRepository>();
services.AddTransient<IHocSinhRepository, HocSinhRepository>();
services.AddTransient<ILopRepository, LopRepository>();
services.AddTransient<IGiaoVienRepository, GiaoVienRepository>();
services.AddTransient<IKhoiRepository, KhoiRepository>();
services.AddTransient<IQuyDinhRepository, QuyDinhRepository>();
services.AddTransient<IDiemRepository, DiemRepository>();
services.AddTransient<IMonHocRepository, MonHocRepository>();
services.AddTransient<IHocKyRepository, HocKyRepository>();
services.AddTransient<IThongBaoRepository, ThongBaoRepository>();


//services.Configure<AppSettings>(builder.Configuration.GetSection("AppSettings"));
//services.Configure<Tokens>(builder.Configuration.GetSection("Tokens"));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseStatusCodePagesWithReExecute("/Home/HandleError/{0}"); //404 notfound
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseAuthentication();
app.UseRouting();
app.UseCors(); //phải có cái này trước caching mới hoạt động
//thứ tự caching phải đặt vậy mới được
//app.Use(async (ctx, next) =>
//{
//    ctx.Request.GetTypedHeaders().CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
//    {
//        Public = true,
//        MaxAge = TimeSpan.FromSeconds(CacheSecondExpires)
//    };
//    await next();
//    //chưa sd
//    //if (ctx.Response.StatusCode == 404 && !ctx.Response.HasStarted)
//    //{
//    //    //Re-execute the request so the user gets the error page
//    //    string originalPath = ctx.Request.Path.Value;
//    //    ctx.Items["originalPath"] = originalPath;
//    //    ctx.Request.Path = "/Home/HandleError/404";
//    //    await next();
//    //}
//});
app.UseResponseCaching();
//end
app.UseAuthorization();
app.UseSession();
app.UseNToastNotify();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Login}/{id?}");
    endpoints.MapRazorPages();
});
//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
