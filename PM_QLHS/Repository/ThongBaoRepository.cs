﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Repository
{
    public interface IThongBaoRepository
    {
        dynamic GetAll();
        dynamic Create(ThongBaoVm vm);
        dynamic Update(ThongBaoVm vm);
        dynamic Delete(int id);
        dynamic GetById(int id);
    }
    public class ThongBaoRepository : IThongBaoRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public ThongBaoRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }

        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<ThongBaoVm>(_connectionString).FindAllBy(masterParams, "sp_ThongBao", "GetAll");
            return results;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaThongBao", id}
            };
            var results = new StoredProcedureFactory<ThongBaoVm>(_connectionString).FindOneBy(masterParams, "sp_ThongBao", "GetById");

            return results;

        }

        public dynamic Create(ThongBaoVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("TenThongBao", vm.TenThongBao);
            masterParams.Add("NoiDungThongBao", vm.NoiDungThongBao);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_ThongBao", "Create");
            return results;
        }

        public dynamic Update(ThongBaoVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaThongBao", vm.MaThongBao);
            masterParams.Add("TenThongBao", vm.TenThongBao);
            masterParams.Add("NoiDungThongBao", vm.NoiDungThongBao);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_ThongBao", "Update");
            return results;
        }

        public dynamic Delete(int id)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaThongBao", id);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_ThongBao", "Delete");
            return results;
        }
    }
}
