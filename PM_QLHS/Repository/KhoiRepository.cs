﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Repository
{
    public interface IKhoiRepository
    {
        dynamic GetAll();
        dynamic GetById(int id);
    }
    public class KhoiRepository: IKhoiRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public KhoiRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }
        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<KhoiVm>(_connectionString).FindAllBy(masterParams, "sp_Khoi", "GetAll");

            var response = new ApiSuccessResult<List<KhoiVm>>();
            if (results.Data != null && results.Data.Items != null)
            {
                response = new ApiSuccessResult<List<KhoiVm>>(results.Data.Items.ToList());
            }

            return response;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaKhoi", id}
            };
            var results = new StoredProcedureFactory<KhoiVm>(_connectionString).FindOneBy(masterParams, "sp_Khoi", "GetById");

            return results;
        }
    }
}
