﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;

namespace PM_QLHS.Repository
{
    public interface ILopRepository
    {
        dynamic GetAll();
        dynamic GetAllView();
        dynamic GetById(int id);
        dynamic Create(LopVm vm);
        dynamic Update(LopVm vm);
        dynamic Delete(int id);
    }
    public class LopRepository : ILopRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public LopRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }
        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<LopVm>(_connectionString).FindAllBy(masterParams, "sp_Lop", "GetAll");

            var response = new ApiSuccessResult<List<LopVm>>();
            if (results.Data != null && results.Data.Items != null)
            {
                response = new ApiSuccessResult<List<LopVm>>(results.Data.Items.ToList());
            }

            return response;
        }

        public dynamic GetAllView()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<LopViewModel>(_connectionString).FindAllBy(masterParams, "sp_Lop", "GetAll");

            return results;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaLop", id}
            };
            var results = new StoredProcedureFactory<LopVm>(_connectionString).FindOneBy(masterParams, "sp_Lop", "GetById");

            return results;
        }

        public dynamic Create(LopVm vm)
        {
            var masterParams = new Dictionary<string, object>();

            if (vm.TenLop != null)
            {
                masterParams.Add("TenLop", vm.TenLop);
            }
            if (vm.MaKhoi != null)
            {
                masterParams.Add("MaKhoi", vm.MaKhoi);
            }
            if (vm.MaGVChuNhiem != null)
            {
                masterParams.Add("MaGVChuNhiem", vm.MaGVChuNhiem);
            }
            if (vm.SiSo != null)
            {
                masterParams.Add("SiSo", vm.SiSo);
            }

            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_Lop", "Create");

            return results;
        }

        public dynamic Update(LopVm vm)
        {
            var masterParams = new Dictionary<string, object>();

            masterParams.Add("MaLop", vm.MaLop);
            if (vm.TenLop != null)
            {
                masterParams.Add("TenLop", vm.TenLop);
            }
            if (vm.MaKhoi != null)
            {
                masterParams.Add("MaKhoi", vm.MaKhoi);
            }
            if (vm.MaGVChuNhiem != null)
            {
                masterParams.Add("MaGVChuNhiem", vm.MaGVChuNhiem);
            }
            if (vm.SiSo != null)
            {
                masterParams.Add("SiSo", vm.SiSo);
            }

            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_Lop", "Update");
            return results;
        }

        public dynamic Delete(int id)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaLop", id);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_Lop", "Delete");
            return results;
        }
    }
}
