﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;

namespace PM_QLHS.Repository
{
    public interface IQuyDinhRepository
    {
        dynamic GetAll();
        dynamic Create(QuyDinhVm vm);
        dynamic Update(QuyDinhVm vm);
        dynamic Delete(int id);
        dynamic GetById(int id);
    }
    public class QuyDinhRepository: IQuyDinhRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public QuyDinhRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }

        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<QuyDinhVm>(_connectionString).FindAllBy(masterParams, "sp_QuyDinh", "GetAll");
            return results;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaQuyDinh", id}
            };
            var results = new StoredProcedureFactory<QuyDinhVm>(_connectionString).FindOneBy(masterParams, "sp_QuyDinh", "GetById");

            return results;

        }

        public dynamic Create(QuyDinhVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("TenQuyDinh", vm.TenQuyDinh);
            masterParams.Add("NoiDungQuyDinh", vm.NoiDungQuyDinh);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_QuyDinh", "Create");
            return results;
        }

        public dynamic Update(QuyDinhVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaQuyDinh", vm.MaQuyDinh);
            masterParams.Add("TenQuyDinh", vm.TenQuyDinh);
            masterParams.Add("NoiDungQuyDinh", vm.NoiDungQuyDinh);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_QuyDinh", "Update");
            return results;
        }

        public dynamic Delete(int id)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaQuyDinh", id);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_QuyDinh", "Delete");
            return results;
        }

    }
}
