﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class;
using PM_QLHS.Models;

namespace PM_QLHS.Repository
{
    public interface ITestRepository
    {
        dynamic GetAll();
    }

    public class TestRepository : ITestRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public TestRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }

        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<TaiKhoanViewModel>(_connectionString).FindAllBy(masterParams, "sp_TaiKhoan", "GetAll");
            return results;
        }
    }
}
