﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Repository
{
    public interface IMonHocRepository
    {
        dynamic GetAll();
    }
    public class MonHocRepository :IMonHocRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public MonHocRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }
        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<MonHocVm>(_connectionString).FindAllBy(masterParams, "sp_MonHoc", "GetAll");

            var response = new ApiSuccessResult<List<MonHocVm>>();
            if (results.Data != null && results.Data.Items != null)
            {
                response = new ApiSuccessResult<List<MonHocVm>>(results.Data.Items.ToList());
            }

            return response;
        }
    }
}
