﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;

namespace PM_QLHS.Repository
{
    public interface IHocSinhRepository
    {
        dynamic GetAll();
        dynamic Create(HocSinhVm vm);
        dynamic Delete(int id);
        dynamic GetById(int id);
        dynamic Update(HocSinhVm vm);
        dynamic GetAllView(string? hoten);
    }

    public class HocSinhRepository : IHocSinhRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public HocSinhRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }

        public dynamic GetAllView(string? hoten)
        {
            var masterParams = new Dictionary<string, object> { };
            masterParams.Add("HoTen", string.IsNullOrEmpty(hoten) ? null : hoten);
            var results = new StoredProcedureFactory<HocSinhViewModel>(_connectionString).FindAllBy(masterParams, "sp_HocSinh", "GetAll");
            return results;
        }

        public dynamic Create(HocSinhVm vm)
        {
            var masterParams = new Dictionary<string, object>();

            if (vm.MaLop != null)
            {
                masterParams.Add("MaLop", vm.MaLop);
            }
            if (vm.HoTen != null)
            {
                masterParams.Add("HoTen", vm.HoTen);
            }
            if (vm.NgaySinh != null)
            {
                masterParams.Add("NgaySinh", vm.NgaySinh);
            }
            if (vm.GioiTinh != null)
            {
                masterParams.Add("GioiTinh", vm.GioiTinh);
            }
            if (vm.Email != null)
            {
                masterParams.Add("Email", vm.Email);
            }
            if (vm.DiaChi != null)
            {
                masterParams.Add("DiaChi", vm.DiaChi);
            }

            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_HocSinh", "Create");

            return results;

            //var response = new ApiSuccessResult<List<HocSinhVm>>("Lưu thất bại");
            //if (results.Data != 0)
            //{
            //    var data = this.GetAll();
            //    var listHS = data.Data.Items as List<HocSinhViewModel>;
            //    var listHSCovert = listHS.Select(x => new HocSinhVm
            //    {
            //        MaLop = x.MaLop,
            //        HoTen = x.HoTen,
            //        NgaySinh = x.NgaySinh,
            //        DiaChi = x.DiaChi,
            //        Email = x.Email,
            //        GioiTinh = x.GioiTinh
            //    }).ToList();
            //    response = new ApiSuccessResult<List<HocSinhVm>>("Lưu thành công", 1, listHSCovert);
            //}
            //return response;
        }

        public dynamic Update(HocSinhVm vm)
        {
            var masterParams = new Dictionary<string, object>();

            if (vm.MaHocSinh != null)
            {
                masterParams.Add("MaHocSinh", vm.MaHocSinh);
            }
            if (vm.MaLop != null)
            {
                masterParams.Add("MaLop", vm.MaLop);
            }
            if (vm.HoTen != null)
            {
                masterParams.Add("HoTen", vm.HoTen);
            }
            if (vm.NgaySinh != null)
            {
                masterParams.Add("NgaySinh", vm.NgaySinh);
            }
            if (vm.GioiTinh != null)
            {
                masterParams.Add("GioiTinh", vm.GioiTinh);
            }
            if (vm.Email != null)
            {
                masterParams.Add("Email", vm.Email);
            }
            if (vm.DiaChi != null)
            {
                masterParams.Add("DiaChi", vm.DiaChi);
            }

            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_HocSinh", "Update");

            return results;
        }

        public dynamic Delete(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaHocSinh", id}
            };
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_HocSinh", "Delete");

            return results;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaHocSinh", id}
            };
            var results = new StoredProcedureFactory<HocSinhVm>(_connectionString).FindOneBy(masterParams, "sp_HocSinh", "GetById");

            return results;

        }

        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<HocSinhVm>(_connectionString).FindAllBy(masterParams, "sp_HocSinh", "GetAll");

            var response = new ApiSuccessResult<List<HocSinhVm>>();
            if (results.Data != null && results.Data.Items != null)
            {
                response = new ApiSuccessResult<List<HocSinhVm>>(results.Data.Items.ToList());
            }

            return response;
        }
    }

}
