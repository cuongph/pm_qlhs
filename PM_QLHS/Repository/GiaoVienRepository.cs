﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;
using PM_QLHS.Class.Dtos;

namespace PM_QLHS.Repository
{
    public interface IGiaoVienRepository
    {
        dynamic GetAll(string? hoten);
        dynamic Create(GiaoVienVm vm);
        dynamic Update(GiaoVienVm vm);
        dynamic Delete(int id);
        dynamic GetById(int id);
        dynamic GetAllGVCN();
    }
    public class GiaoVienRepository : IGiaoVienRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public GiaoVienRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }

        public dynamic GetAll(string? hoten)
        {
            var masterParams = new Dictionary<string, object> { };
            masterParams.Add("HoTen", string.IsNullOrEmpty(hoten) ? null : hoten);
            var results = new StoredProcedureFactory<GiaoVienBindingVm>(_connectionString).FindAllBy(masterParams, "sp_GiaoVien", "GetAll");
            return results;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaGiaoVien", id}
            };
            var results = new StoredProcedureFactory<GiaoVienVm>(_connectionString).FindOneBy(masterParams, "sp_GiaoVien", "GetById");

            return results;

        }

        public dynamic Create(GiaoVienVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("HoTen", vm.HoTen);
            masterParams.Add("ChucVu", vm.ChucVu);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_GiaoVien", "Create");
            return results;
        }

        public dynamic Update(GiaoVienVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaGiaoVien", vm.MaGiaoVien);
            masterParams.Add("HoTen", vm.HoTen);
            masterParams.Add("ChucVu", vm.ChucVu);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_GiaoVien", "Update");
            return results;
        }

        public dynamic Delete(int id)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaGiaoVien", id);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_GiaoVien", "Delete");
            return results;
        }

        public dynamic GetAllGVCN()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<GiaoVienVm>(_connectionString).FindAllBy(masterParams, "sp_GiaoVien", "GetAllGVCN");

            var response = new ApiSuccessResult<List<GiaoVienVm>>();
            if (results.Data != null && results.Data.Items != null)
            {
                response = new ApiSuccessResult<List<GiaoVienVm>>(results.Data.Items.ToList());
            }

            return response;
        }
    }
}
