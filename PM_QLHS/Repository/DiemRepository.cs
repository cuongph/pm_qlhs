﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class.ViewModels;
using PM_QLHS.Models;

namespace PM_QLHS.Repository
{
    public interface IDiemRepository
    {
        dynamic GetAllView(TraCuuDiemVm request);
        dynamic GetById(int id);
        dynamic Create(DiemVm vm);
        dynamic Update(DiemVm vm);
        dynamic Delete(int id);
    }
    public class DiemRepository: IDiemRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public DiemRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }
        public dynamic GetAllView(TraCuuDiemVm vm)
        {
            var masterParams = new Dictionary<string, object> { };

            if (vm.MaLop != null)
            {
                masterParams.Add("MaLop", vm.MaLop);
            }
            if (vm.MaHocSinh != null)
            {
                masterParams.Add("MaHocSinh", vm.MaHocSinh);
            }

            var results = new StoredProcedureFactory<DiemViewModel>(_connectionString).FindAllBy(masterParams, "sp_Diem", "GetAll");

            return results;
        }

        public dynamic GetById(int id)
        {
            var masterParams = new Dictionary<string, object> {
                {"MaDiem", id}
            };
            var results = new StoredProcedureFactory<DiemVm>(_connectionString).FindOneBy(masterParams, "sp_Diem", "GetById");

            return results;
        }

        public dynamic Create(DiemVm vm)
        {
            var masterParams = new Dictionary<string, object>();

            if (vm.Diem15Phut != null)
            {
                masterParams.Add("Diem15Phut", vm.Diem15Phut);
            }
            if (vm.Diem1Tiet != null)
            {
                masterParams.Add("Diem1Tiet", vm.Diem1Tiet);
            }
            if (vm.DiemHocKy != null)
            {
                masterParams.Add("DiemHocKy", vm.DiemHocKy);
            }
            if (vm.MaMonHoc != null)
            {
                masterParams.Add("MaMonHoc", vm.MaMonHoc);
            }
            if (vm.MaHocSinh != null)
            {
                masterParams.Add("MaHocSinh", vm.MaHocSinh);
            }
            if (vm.MaHocKy != null)
            {
                masterParams.Add("MaHocKy", vm.MaHocKy);
            }

            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_Diem", "Create");

            return results;
        }

        public dynamic Update(DiemVm vm)
        {
            var masterParams = new Dictionary<string, object>();
            if (vm.MaDiem != null)
            {
                masterParams.Add("MaDiem", vm.MaDiem);
            }
            if (vm.Diem15Phut != null)
            {
                masterParams.Add("Diem15Phut", vm.Diem15Phut);
            }
            if (vm.Diem1Tiet != null)
            {
                masterParams.Add("Diem1Tiet", vm.Diem1Tiet);
            }
            if (vm.DiemHocKy != null)
            {
                masterParams.Add("DiemHocKy", vm.DiemHocKy);
            }
            if (vm.MaMonHoc != null)
            {
                masterParams.Add("MaMonHoc", vm.MaMonHoc);
            }
            if (vm.MaHocSinh != null)
            {
                masterParams.Add("MaHocSinh", vm.MaHocSinh);
            }
            if (vm.MaHocKy != null)
            {
                masterParams.Add("MaHocKy", vm.MaHocKy);
            }

            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_Diem", "Update");

            return results;
        }

        public dynamic Delete(int id)
        {
            var masterParams = new Dictionary<string, object>();
            masterParams.Add("MaDiem", id);
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_Diem", "Delete");
            return results;
        }
    }
}
