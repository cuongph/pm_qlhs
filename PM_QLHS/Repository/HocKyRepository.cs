﻿using Microsoft.Extensions.Options;
using PM_QLHS.Class;
using PM_QLHS.Class.Dtos;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class.ViewModels;

namespace PM_QLHS.Repository
{
    public interface IHocKyRepository
    {
        dynamic GetAll();
    }
    public class HocKyRepository: IHocKyRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public HocKyRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }
        public dynamic GetAll()
        {
            var masterParams = new Dictionary<string, object> { };
            var results = new StoredProcedureFactory<HocKyVm>(_connectionString).FindAllBy(masterParams, "sp_HocKy", "GetAll");

            var response = new ApiSuccessResult<List<HocKyVm>>();
            if (results.Data != null && results.Data.Items != null)
            {
                response = new ApiSuccessResult<List<HocKyVm>>(results.Data.Items.ToList());
            }

            return response;
        }
    }
}
