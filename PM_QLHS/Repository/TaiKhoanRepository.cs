﻿using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.Extensions.Options;
using PM_QLHS.Class.Helper;
using PM_QLHS.Class;
using PM_QLHS.Models;
using PM_QLHS.Class.ViewModels;
using System.Reflection;
using PM_QLHS.Class.Dtos;

namespace PM_QLHS.Repository
{
    public interface ITaiKhoanRepository
    {
        dynamic GetByID(string username);
        dynamic Register(TaiKhoanVm vm);
        dynamic Authenticate(TaiKhoanVm vm);
        dynamic ChangePass(TaiKhoanVm vm);
        dynamic Update(TaiKhoanVm vm);

    }
    public class TaiKhoanRepository : ITaiKhoanRepository
    {
        private readonly string _connectionString;
        private readonly ConnectionStrings _connectionStrings;

        public TaiKhoanRepository(IConfiguration configuration, IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;
            _connectionString = _connectionStrings.ConnectionString;
        }

        public dynamic GetByID(string username)
        {
            var masterParams = new Dictionary<string, object> { };
            masterParams.Add("TenTaiKhoan", username);
            var results = new StoredProcedureFactory<TaiKhoanVm>(_connectionString).FindOneBy(masterParams, "sp_TaiKhoan", "GetByID");
            return results;
        }

        public dynamic Register(TaiKhoanVm vm)
        {
            var masterParams = new Dictionary<string, object> {
                {"TenTaiKhoan", vm.TenTaiKhoan},
                {"MatKhau", BCryptNet.HashPassword(vm.MatKhau)},
                {"LoaiTaiKhoan", vm.LoaiTaiKhoan},
                {"MaGiaoVien", vm.MaGiaoVien}
            };
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_TaiKhoan", "Create");
            return results;
        }

        public dynamic Authenticate(TaiKhoanVm vm)
        {
            var result = new ApiResult<TaiKhoanVm>();
            var user = new TaiKhoanVm();
            var res = this.GetByID(vm.TenTaiKhoan);
            // validate
            user = res.Success ? res.Data : null;
            if (user == null || !BCryptNet.Verify(vm.MatKhau, user.MatKhau))
            {
                result = new ApiErrorResult<TaiKhoanVm>("Tài khoản đăng nhập hoặc mật khẩu không chính xác");
            }
            else
            {
                result = new ApiSuccessResult<TaiKhoanVm>(user);
            }
            return result;
        }

        public dynamic ChangePass(TaiKhoanVm vm)
        {
            var result = new ApiResult<int>();
            var user = new TaiKhoanVm();
            var res = this.GetByID(vm.TenTaiKhoan);
            // validate
            user = res.Success ? res.Data : null;
            if (user == null || !BCryptNet.Verify(vm.MatKhauCu, user.MatKhau))
            {
                result = new ApiErrorResult<int>(0);
            }
            else
            {
                var masterParams = new Dictionary<string, object> {
                    {"MaTaiKhoan", user.MaTaiKhoan},
                    {"MatKhau", BCryptNet.HashPassword(vm.MatKhau)}
                };
                result = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_TaiKhoan", "ChangePassword");
                
            }
            return result;
        }

        public dynamic Update(TaiKhoanVm vm)
        {
            var masterParams = new Dictionary<string, object> {
                {"LoaiTaiKhoan", vm.LoaiTaiKhoan},
                {"MaTaiKhoan", vm.MaTaiKhoan}
            };
            var results = new StoredProcedureFactory<int>(_connectionString).intExecute(masterParams, "sp_TaiKhoan", "Update");
            return results;
        }

    }
}
