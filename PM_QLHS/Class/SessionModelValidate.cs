﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PM_QLHS.Class
{
    public class SessionModelValidate : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var sessions = context.HttpContext.Session.GetString("MemberSession");
            if (sessions == null)
            {
                context.Result = new RedirectToActionResult("SessionTimeout", "Home", null);
            }
            base.OnActionExecuting(context);
        }
    }
}
