﻿namespace PM_QLHS.Class.ViewModels
{
    public class QuyDinhVm
    {
        public int MaQuyDinh { get; set; }
        public string? TenQuyDinh { get; set; }
        public string? NoiDungQuyDinh { get; set; }
    }
}
