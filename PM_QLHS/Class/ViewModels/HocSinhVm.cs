﻿namespace PM_QLHS.Class.ViewModels
{
    public class HocSinhVm
    {
        public int? MaHocSinh { get; set; }
        public int? MaLop { get; set; }
        public string? HoTen { get; set; }
        public DateTime? NgaySinh { get; set; }
        public bool? GioiTinh { get; set; }
        public string? Email { get; set; }
        public string? DiaChi { get; set; }
    }
}
