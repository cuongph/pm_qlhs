﻿namespace PM_QLHS.Class.ViewModels
{
    public class KhoiVm
    {
        public int MaKhoi { get; set; }
        public string? TenKhoi { get; set; }
    }
}
