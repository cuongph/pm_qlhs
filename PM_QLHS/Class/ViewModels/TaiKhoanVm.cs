﻿namespace PM_QLHS.Class.ViewModels
{
    public class TaiKhoanVm
    {
        public int MaTaiKhoan { get; set; }
        public string TenTaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string MatKhauCu { get; set; }
        public int LoaiTaiKhoan { get; set; }
        public int MaGiaoVien { get; set; }
    }
}
