﻿namespace PM_QLHS.Class.ViewModels
{
    public class MonHocVm
    {
        public int MaMonHoc { get; set; }
        public string? TenMonHoc { get; set; }
    }
}
