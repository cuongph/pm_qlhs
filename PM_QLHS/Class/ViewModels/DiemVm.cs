﻿namespace PM_QLHS.Class.ViewModels
{
    public class DiemVm
    {
        public int? MaBangDiemMon { get; set; }
        public int? MaMonHoc { get; set; }
        public int? MaHocSinh { get; set; }
        public int? MaDiem { get; set; }
        public int? MaHocKy { get; set; }
        public float? Diem15Phut { get; set; }
        public float? Diem1Tiet { get; set; }
        public float? DiemHocKy { get; set; }
    }
}
