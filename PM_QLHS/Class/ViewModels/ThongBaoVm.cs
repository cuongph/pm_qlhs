﻿namespace PM_QLHS.Class.ViewModels
{
    public class ThongBaoVm
    {
        public int MaThongBao { get; set; }
        public string? TenThongBao { get; set; }
        public string? NoiDungThongBao { get; set; }
        public string? NgayThongBao { get; set; }
    }
}
