﻿namespace PM_QLHS.Class.ViewModels
{
    public class HocKyVm
    {
        public int MaHocKy { get; set; }
        public string? TenHocKy { get; set; }
    }
}
