﻿namespace PM_QLHS.Class.ViewModels
{
    public class GiaoVienVm
    {
        public int MaGiaoVien { get; set; }
        public string? HoTen { get; set; }
        public string? ChucVu { get; set; }
        public string? TenChucVu { get; set; }

    }
}
