﻿namespace PM_QLHS.Class.ViewModels
{
    public class LopVm
    {
        public int MaLop { get; set; }
        public string? TenLop { get; set; }
        public int? SiSo { get; set; }
        public int? MaKhoi { get; set; }
        public int? MaGVChuNhiem { get; set; } 
        public string? ThoiKhoaBieu { get; set; }

    }
}
