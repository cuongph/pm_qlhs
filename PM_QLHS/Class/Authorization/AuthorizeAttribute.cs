﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using PM_QLHS.Class.ViewModels;
using System.Data;

namespace PM_QLHS.Class.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly int _role; //truyền 1 role thôi ko cần list
        public AuthorizeAttribute(int role)
        {
            _role = role;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // skip authorization if action is decorated with [AllowAnonymous] attribute
            var allowAnonymous = context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any();
            if (allowAnonymous)
                return;
            // authorization
            var user = context.HttpContext.Session.GetString(SystemConstants.MemberSession) != null ? JsonConvert.DeserializeObject<TaiKhoanVm>(context.HttpContext.Session.GetString(SystemConstants.MemberSession)) : null;
            if (user == null || !roleAuth(user.LoaiTaiKhoan, _role))
            {
                context.Result = new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "Home",
                        action = "Forbidden"
                    })
                );
            }
        }

        private bool roleAuth(int userRole, int requiredRole)
        {
            if (userRole == requiredRole || userRole == 0)
            {
                return true; //ok
            }
            else
            {
                return false;
            }
        }
    }
}
