﻿using Microsoft.AspNetCore.Http;

namespace PM_QLHS.Class.Authorization
{
    public class Role
    {
        public const int Admin = 0;
        public const int BGH = 1;
        public const int GVien = 2;
        public const int GVu = 3;
    }
    public class PageDefault
    {
        public List<string> Pages { get; set; }
        public PageDefault()
        {
            Pages = new List<string> { "HocSinh", "QuyDinh", "Diem", "GiaoVien" };
        }
    }
}
