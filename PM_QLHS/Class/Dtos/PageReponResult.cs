﻿using System.Collections.Generic;
namespace PM_QLHS.Class.Dtos
{
    public class PageReponResult<T> : PagedReponBase
    {
        public IEnumerable<T> Items { set; get; }
    }
}
